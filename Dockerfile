# Use the official WordPress image as the base image
FROM wordpress:latest

# Copy your WordPress files into the container
COPY . /var/www/html

# Set the working directory
WORKDIR /var/www/html

# Expose port 80 for HTTP
EXPOSE 80

# Start the WordPress application
CMD ["apache2-foreground"]
